from django.contrib import admin
from events.models import Category, Event


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
admin.site.register(Category,CategoryAdmin)


class EventAdmin(admin.ModelAdmin):
    list_display = ('title','description','start_date','end_date','location','image','published')
admin.site.register(Event,EventAdmin)