from __future__ import unicode_literals
from django.db import models
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal


class Category(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'category'
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ('name',)
        
    class Admin:
        list_display = ('name',)

    def __unicode__(self):
        return self.name


class Event(models.Model):
    title = models.CharField(max_length=128)
    description = models.CharField(max_length=128)
    location = models.CharField(max_length=128)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    image = models.ImageField(max_length=128)
    category = models.ManyToManyField(Category)
    published = models.BooleanField(default=False)

    class Meta:
        db_table = 'event'
        verbose_name = _('event')
        verbose_name_plural = _('events')
        ordering = ('title',)
        
    class Admin:
        list_display = ('title',)

    def __unicode__(self):
        return self.title
