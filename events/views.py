from django.shortcuts import render
from django.conf import settings
from events.models import Event, Category
import datetime
from django.db.models import Q


def index(request):
    today = datetime.date.today()
    events = Event.objects.filter(published=True,start_date__year=today.year,start_date__month=today.month,start_date__day=today.day)
    categories = Category.objects.all()

    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    category = request.GET.get('category')
    query = request.GET.get('query')

    if query:
        events = Event.objects.filter(Q(title__icontains=query) |
                                     Q(location__icontains=query)
                                    )

    if start_date and end_date:
        try:
            start_date = datetime.datetime.strptime(start_date, '%m/%d/%Y').date()
            end_date = datetime.datetime.strptime(end_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)            
        except ValueError:
            date_error = "yes"  
        filter_date_period = True

    if start_date:
    	events = Event.objects.filter(published=True,start_date__gte=start_date)

    if end_date:
    	events = Event.objects.filter(published=True,end_date__lte=end_date)

    if start_date and end_date:
    	events = Event.objects.filter(published=True,start_date__gte=start_date,end_date__lte=end_date)
    name = None
    if category:
        name = Category.objects.get(pk=category).name
    	events = Event.objects.filter(published=True,category=category)
        print events

    context = {
        "title" : "Events",
        "events" : events,
        "categories" : categories,
        "start_date" : start_date,
        "end_date" : end_date,
        "category": name,
        "query" : query
    }
    return render(request,'index.html',context)